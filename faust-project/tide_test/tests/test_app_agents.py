import pytest
from unittest.mock import Mock, patch

from tide_test.app import app
from tide_test.company_view.models import Company
from tide_test.fetcher import ClearbitFetcher
from tide_test.company_view.agents import store_company_views, store_domain_views

@pytest.fixture()
def test_app(event_loop):
    """passing in event_loop helps avoid 'attached to a different loop' error"""
    app.finalize()
    app.conf.store = 'memory://'
    app.flow_control.resume()
    return app

@pytest.mark.asyncio()
async def test_store_company_views(test_app):
    with patch(__name__ + '.store_domain_views') as mocked_domain:
        mocked_domain.send = mock_coro()
        async with store_company_views.test_context() as agent:
            company = Company(name='Tide', domain='tide.co')
            fetcher = ClearbitFetcher().get_company(domain=company.domain)
            await agent.put(company)
            mocked_domain.send(fetcher)
            mocked_domain.send.assert_called_with(fetcher)

def mock_coro(return_value=None, **kwargs):
    """Create mock coroutine function."""
    async def wrapped(*args, **kwargs):
        return return_value
    return Mock(wraps=wrapped, **kwargs)

@pytest.mark.asyncio()
async def test_store_domain_views(test_app):
    async with store_domain_views.test_context() as agent:
        event = await agent.put(ClearbitFetcher().get_company(domain='tide.co'))
        assert 'age' in agent.results[event.message.offset] and 'employees' in agent.results[event.message.offset]