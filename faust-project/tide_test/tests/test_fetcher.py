import pytest
from unittest.mock import MagicMock

from tide_test.fetcher import ClearbitFetcher

def test_fetcher_mocked():
    fetcher = ClearbitFetcher()
    fetcher.get_company = MagicMock(return_value=None)
    fetcher.get_company('tide.co')
    fetcher.get_company.assert_called_with('tide.co')


def test_fetcher_existing():
    fetcher = ClearbitFetcher().get_company(domain='tide.co')
    assert 'domain' in fetcher

def test_fetcher_nonexisting():
    with pytest.raises(ValueError):
        raise ClearbitFetcher().get_company(domain='nonexisting.com')