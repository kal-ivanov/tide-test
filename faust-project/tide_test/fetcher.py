import clearbit

from simple_settings import settings

class ClearbitFetcher:

    def __init__(self, key=settings.CLEARBIT_API_KEY):
        self.key = key
    
    def get_company(self, domain):
        try:
            clearbit.key = self.key
            company = clearbit.Company.find(domain=domain, stream=True)
            if company != None:
                return company
            else:
                raise ValueError
        except Exception as e:
            return e