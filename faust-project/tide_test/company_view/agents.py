import logging
import datetime

from tide_test.app import app
from tide_test.company_view.models import Company
from faust.web import Request, Response, View
from tide_test.fetcher import ClearbitFetcher

companies_topic = app.topic('companies', partitions=8, value_type=Company)
domains_topic = app.topic('domain_information', partitions=8, value_serializer='json')

messages = [] # use as in-memory domain-base will be wiped on restart
domains_info = {} # dict used as in-memory table, will be wiped on restart

@app.agent(companies_topic)
async def store_company_views(companies):
    async for company in companies:
        print(f"received message for company {company.name}")
        messages.append(company)
        try:
            fetcher = ClearbitFetcher().get_company(domain=company.domain)
            await domains_topic.send(value=fetcher)
        except Exception as e:
            print(e)
        yield company

@app.agent(domains_topic)
async def store_domain_views(domain_information):
    async for domain in domain_information:
        age = datetime.date.today().year - int(domain['foundedYear'])
        print(f"received message for company {domain['name']} with {domain['metrics']['employees']} employees and age of the company {age} year/s.")
        domains_info[domain['name']] = {'employees': domain['metrics']['employees'], 'age': age}
        yield domains_info[domain['name']]

@app.page('/companies/')
class counter(View):
    async def get(self, request):
        return self.json({'messages': messages})

    async def post(self, request):
        body = await request.json()
        await companies_topic.send(value=body)
        return self.json({'processed': True})

@app.page('/companies/{name}/')
class counter(View):
    async def get(self, request, name):
        try:
            return self.json({'domain_information': [{name: domains_info[name]}]})
        except:
            return self.json({'domain_information': 'Not Found'}, status=404)


logger = logging.getLogger(__name__)



